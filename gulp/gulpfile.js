'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    //jshint = require('gulp-jshint'),
    //svgSprite = require("gulp-svg-sprite"),
    browserSync = require('browser-sync').create(),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    stripCssComments = require('gulp-strip-css-comments'),
    plumber = require('gulp-plumber');

// Change variable to your localhost url
var host = 'mage2-template.local';

/*********
 * PATHS
 ********/
var path = {
    scss: './../app/design/frontend/Wstechlab/default/web/scss/**/*.scss',
    cssDestModule: './../app/design/frontend/Wstechlab/default/web/css',
    cssDestStatic: './../pub/static/frontend/Wstechlab/default/en_US/css',
    cssDestStaticIt: './../pub/static/frontend/Wstechlab/default/it_IT/css',
    browserSyncCss: './../app/design/frontend/Wstechlab/default/web/css/**/*.css',
    browserSyncStaticCss: './../pub/static/frontend/Wstechlab/default/en_US/css/**/*.css'
};


/** Browser Sync **/
/******************/

// Files to inject
var files = [
    path.browserSyncCss,
    path.browserSyncStaticCss
    //'./../js/**/*.js'
];

browserSync.init(files, {
    open: 'external',
    host: host,
    proxy: host,
    port: 80 // for work mamp
});

/** Browser Sync **/
/****** End ******/


// Task for Browser Sync
/* gulp.task('browser-sync', function () {
   return browserSync.reload({
       stream: true
   })
}); */


// SASS
gulp.task('sass', function () {
    return gulp.src(path.scss)
        .pipe(plumber(function (error) {
            console.log('Plumber sass error compile: ', error);
        }))
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(stripCssComments())
        .pipe(autoprefixer({
                browsers: ['last 2 versions', 'Explorer >= 10', 'Android >= 4.1', 'Safari >= 7', 'iOS >= 7', 'Firefox >= 20'],
                cascade: false
            })
        )
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.cssDestModule))
        .pipe(gulp.dest(path.cssDestStatic))
        .pipe(gulp.dest(path.cssDestStaticIt))
        .pipe(browserSync.reload({
            stream: true
        }));
});

// Watch
gulp.task('watch', ['sass'], function () {
    // Watch .scss files
    gulp.watch(path.scss, ['sass']);

    // Watch .svg files
    // gulp.watch('icons/**/*.svg', ['svgSprite']);
});


// define the default task and add the watch task to it
gulp.task('default', ['watch']);
